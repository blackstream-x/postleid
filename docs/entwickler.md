# Für Entwicker

## Vorbereitungen

Das Repository klonen:

```bash
git clone https://codeberg.org/blackstream-x/postleid.git
```

Ins Verzeichnis `postleid` wechseln und dort eine virtuelle Umgebung
erzeugen und aktivieren (siehe "Allgemeines").

Abhängigkeiten installieren:

```bash
pip install -r requirements.txt
```

Danach kann postleid über das Wrapper-Skript `pl_wrapper.py`
aufgerufen werden.


## Tests durchführen

_(tba)_
