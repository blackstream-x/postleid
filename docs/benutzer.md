# Für Benutzer

## Installation

In einem aktiven Python Virtual Environment (siehe "Allgemeines")
werden mit dem Befehl

```bash
pip install -U postleid
```

das Python-Paket und seine Abhängigkeiten installiert.
Dabei werden mehrere Python-Pakte aus dem Internet heruntergeladen,
was ein paar Sekunden dauern kann.

Zusätzlich wird in der virtuellen Umgebung der Befehl
`postleid` bereitgestellt.


## Aufruf

Grundsätzlich kann das Skript mit einem der Befehle `postleid`
oder `python -m postleid` aufgerufen werden.

Es erwartet als Parameter mindestens den Namen der zu prüfenden
bzw. zu korrigierenden Exceldatei (z.B. als relative Pfadangabe).

!!! note "Originaldaten werden nicht überschrieben"

    Die ursprüngliche Datei wird nie überschrieben.
    Falls Korrekturen vorgenommen werden konnten,
    erfolgt die Ausgabe immer in eine andere Datei.
    Standardmäßig wird dem ursprünglichen Dateinamen dafür
    `fixed-` vorangestellt.

### integrierte Hilfe

Der Aufruf `postleid -h` (bzw. `python -m postleid -h`)
gibt eine kurze Übersicht der möglichen Teilbefehle aus:

```text
Aufruf: postleid [-h] [-v | -q] {version,list,fix} ...

Postleitzahlen in Excel-Dateien korrigieren

Optionen:
  -h, --help          diese Meldung anzeigen und beenden

Logging-Optionen:
  steuern die Meldungsausgaben (Standard-Loglevel: INFO)

  -v, --verbose       alle Meldungen ausgeben (Loglevel DEBUG)
  -q, --quiet         nur Warnungen und Fehler ausgeben (Loglevel WARNING)

Teilbefehle:
  {version,list,fix}  'version': Version ausgeben und beenden; 'list': Länder
                      bzw. Regeln ausgeben und beenden; 'fix': Daten
                      korrigieren. Hilfe zum jeweiligen Aufruf mit
                      nachfolgendem -h

```

## Verwendung zum Korrigieren von Postleitzahlen

Teilbefehl `fix` mit Angabe der Exceldatei
und optional der Datei, in die die Ausgabe erfolgen soll 
(als Standard wird eine Datei mit im gleichen Pfad wie die Originaldatei
mit dem Namen vorangestelltem `fixed-` verwendet).

!!! note ""
    Falls die Zieldatei bereits existiert, wird sie ohne Rückfrage überschrieben.

### Einstellungen

Benutzereinstellungen werden standardmäßig aus der Datei
`postleid-settings.yaml` im aktuellen Verzeichnis gelesen.

Falls diese noch nicht existiert, wird sie beim ersten
Aufruf des Skripts mit diesem Teilbefehl (ausgenommen Aufrufe mit `-h`)
neu angelegt.

Es handelt sich dabei um eine ausführlich kommentierte Textdatei,
über die das Verhalten des Skripts über zz. 4 Parameter gesteuert werden kann.

Beschreibung der Parameter:

default_country_code
:   ISO-3166-Alpha-2-Country-Code des Landes, da als Standardziel angenommen wird,
    wenn die Tabelle entweder kein Land enthält oder in der jeweiligen
    Zeile keine Länderangabe enthalten ist. Standardwert: `de`

_(tbc)_

### Bespiel eines Aufrufs

```text
(venv) user@linux:~/projects/postleid$ postleid fix testdata/fehlerhafte-daten.xlsx
INFO     | Lade Einstellungen aus postleid-settings.yaml …
INFO     | … ok
INFO     | ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
INFO     | Lade Datei [...]/testdata/fehlerhafte-daten.xlsx …
INFO     | … ok
WARNING  |     2  →  Originalwert: 0
WARNING  |       (✘) außerhalb des Bereichs - 0 is below defined minimum of
WARNING  | 1000
WARNING  |     5  →  Originalwert: 77
WARNING  |       (✘) außerhalb des Bereichs - 77 is below defined minimum of
WARNING  | 1000
WARNING  |     6  →  Originalwert: '10,675'
WARNING  |       (✘) außerhalb des Bereichs - 10 is below defined minimum of
WARNING  | 1000
WARNING  |     8  →  Originalwert: '10.345'
WARNING  |       (✘) außerhalb des Bereichs - 10 is below defined minimum of
WARNING  | 1000
WARNING  |     9  →  Originalwert: 10.014
WARNING  |       (✘) außerhalb des Bereichs - 10 is below defined minimum of
WARNING  | 1000
WARNING  |    12  →  Originalwert: 99.998
WARNING  |       (✘) außerhalb des Bereichs - 99 is below defined minimum of
WARNING  | 1000
WARNING  | ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
INFO     | Ergebnis: 13 Datensätze verarbeitet, davon:
INFO     |  (=) unverändert:            1
INFO     |  (✔) korrigiert:             6
WARNING  |  (✘) außerhalb des Bereichs: 6
INFO     | ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
WARNING  | Da die Orignaldaten nicht fehlerfrei waren, wurden sie nicht nach
WARNING  | Land und Postleitzahl sortiert.
INFO     | Schreibe Ausgabedatei
INFO     | [...]/testdata/fixed-fehlerhafte-daten.xlsx …
INFO     | … ok
(venv) user@linux:~/projects/postleid$
```

