# Für Benutzer

## Installation

In einem aktiven Python Virtual Environment (siehe "Allgemeines")
werden mit dem Befehl

```bash
pip install -U postleid
```

das Python-Paket und seine Abhängigkeiten installiert.
Dabei werden mehrere Python-Pakte aus dem Internet heruntergeladen,
was ein paar Sekunden dauern kann.

Zusätzlich wird in der virtuellen Umgebung der Befehl
`postleid` bereitgestellt.


## Aufruf

Grundsätzlich kann das Skript mit einem der Befehle `postleid`
oder `python -m postleid` aufgerufen werden.

Es erwartet als Parameter mindestens den Namen der zu prüfenden
bzw. zu korrigierenden Exceldatei (z.B. als relative Pfadangabe).

!!! note "Originaldaten werden nicht überschrieben"

    Die ursprüngliche Datei wird nie überschrieben.
    Falls Korrekturen vorgenommen werden konnten,
    erfolgt die Ausgabe immer in eine andere Datei.
    Standardmäßig wird dem ursprünglichen Dateinamen dafür
    `fixed-` vorangestellt.

### integrierte Hilfe

Der Aufruf `postleid -h` (bzw. `python -m postleid -h`)
gibt eine kurze Übersicht der möglichen Teilbefehle aus:

```text
Aufruf: postleid [-h] [--version] [-v | -q] {list,fix} ...

Postleitzahlen in Excel-Dateien korrigieren

Optionen:
  -h, --help     diese Meldung anzeigen und beenden
  --version      Version anzeigen und beenden

Logging-Optionen:
  steuern die Meldungsausgaben (Standard-Loglevel: INFO)

  -v, --verbose  alle Meldungen ausgeben (Loglevel DEBUG)
  -q, --quiet    nur Warnungen und Fehler ausgeben (Loglevel WARNING)

Teilbefehle:
  {list,fix}     'list': Länder bzw. Regeln ausgeben und beenden; 'fix': Daten
                 korrigieren. Hilfe zum jeweiligen Aufruf mit nachfolgendem -h

```

## Verwendung zum Korrigieren von Postleitzahlen

Teilbefehl `fix` mit Angabe der Exceldatei
und optional der Datei, in die die Ausgabe erfolgen soll 
(als Standard wird eine Datei mit im gleichen Pfad wie die Originaldatei
mit dem Namen vorangestelltem `fixed-` verwendet).

!!! note ""
    Falls die Zieldatei bereits existiert, wird sie ohne Rückfrage überschrieben.

```text
Aufruf: postleid fix [-h] [-g] [-o AUSGABEDATEI] [-s EINSTELLUNGSDATEI]
                     EXCELDATEI

Postleitzahlen in Excel-Dateien korrigieren

Positionsparameter:
  EXCELDATEI            die Original-Exceldatei

Optionen:
  -h, --help            diese Meldung anzeigen und beenden
  -g, --guess-1000s     Postleitzahlen unter 1000 mit 1000 multiplizieren
  -o AUSGABEDATEI, --output-file AUSGABEDATEI
                        die Ausgabedatei (Standardwert: Name der Original-
                        Exceldatei mit vorangestelltem 'fixed-')
  -s EINSTELLUNGSDATEI, --settings-file EINSTELLUNGSDATEI
                        die Datei mit Benutzereinstellungen (Standardwert:
                        postleid-settings.yaml im aktuellen Verzeichnis)
```


### Einstellungen

Benutzereinstellungen werden standardmäßig aus der Datei
`postleid-settings.yaml` im aktuellen Verzeichnis gelesen.

Falls diese noch nicht existiert, wird sie beim ersten
Aufruf des Skripts mit diesem Teilbefehl (ausgenommen Aufrufe mit `-h`)
neu angelegt.

Es handelt sich dabei um eine ausführlich kommentierte Textdatei,
über die das Verhalten des Skripts über zz. 4 Parameter gesteuert werden kann.

Beschreibung der Parameter:

default_country_code
:   String mit dem [ISO-3166-Alpha-2-Country-Code][iso3166alpha2]
    des Landes, da als Standardziel angenommen wird,
    wenn die Tabelle entweder kein Land enthält oder in der jeweiligen
    Zeile keine Länderangabe enthalten ist. Standardwert: `"de"`

guess_1000s
:   Boolescher Wert (true/yes oder false/no), der angibt, ob bei
    rein numerischen Postleitzahlen (inkusive Dezimalbrüche) unter 1000
    der Wert mit **1000** multipliziert werden soll.
    Das kann hilfreich sein, wenn Postleitzahlen im Ursprungsdokument
    als Dezimalbrüche (z.B. `12.345`) gespeichert sind – insbesondere
    Excel ist ja für seinen kreativen Umgang mit Zahlenformaten bekannt.
    Da es Länder mit 3-stelligen Postleitzahlen gibt
    (Guinea, Island und weitere), wurde hier der Standard auf `no` gesetzt.
    Die Multiplikation kann mit der Option `--guess-1000s` bzw `-g` immer
    erzwungen werden.
    
country_headings
:   Liste von Strings, die eine möglicherweise vorhandene Tabellenspalte
    mit dem Namen des Landes identifizieren.
    Die erste Tabellenspalte, deren Überschrift mit einem dieser Strings
    übereinstimmt, wird als Spalte mit den Ländernamen angenommen.
    Groß- und Kleinschreibung ist dabei irrelevant.
    Standardwert: `["Land", "Staat", "Country"]`

postal_code_heading_parts
:   Liste von _Teilstrings_, welche die Tabellenspalte mit den Postleitzahlen
    identifizieren. Die erste Tabellenspalte, deren Überschrift einen dieser
    Teilstrings enthält, wird als Spalte mit den Postleitzahlen angenommen.
    Groß- und Kleinschreibung ist dabei irrelevant.
    Standardwert: `["PLZ", "Postleit", "Zip Code"]`


### Bespiel eines Aufrufs

```text
(venv) user@linux:~/projects/postleid$ postleid fix --guess-1000s testdata/fehlerhafte-daten.xlsx
INFO     | Lade Einstellungen aus postleid-settings.yaml …
INFO     | … ok
INFO     | –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
INFO     | Lade Datei (…)/testdata/fehlerhafte-daten.xlsx …
INFO     | … ok
WARNING  |     2  →  Originalwert: '0'
WARNING  |       (✘) außerhalb des Bereichs - 0 is below defined minimum of 1000
WARNING  | –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
INFO     | Ergebnis: 13 Datensätze verarbeitet, davon:
INFO     |  (=) unverändert:            6
INFO     |  (✔) korrigiert:             6
WARNING  |  (✘) außerhalb des Bereichs: 1
INFO     | –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
WARNING  | Da die Orignaldaten nicht fehlerfrei waren, wurden sie nicht nach Land und Postleitzahl sortiert.
INFO     | Schreibe Ausgabedatei (…)/testdata/fixed-fehlerhafte-daten.xlsx …
INFO     | … ok
(venv) user@linux:~/projects/postleid$
```

---
[iso3166alpha2]: https://de.wikipedia.org/wiki/ISO-3166-1-Kodierliste
