# Allgemeines

_Python-Skript zum Korrigieren von Postleitzahlen in Excel-Dateien_

Das Skript setzt Python Version 3 voraus
und kann über PyPI als Package bezogen werden
(siehe <https://pypi.org/project/postleid/>)

Grundsätzlich sollten die Installation und der Aufruf in einem
aktiven Python Virtual Environment erfolgen.


## Virtual Environment erzeugen

Eine virtuelle Umgebung wird in Python 3 mit dem Befehl

`python3 -m venv Umgebungsname`

erzeugt. Dabei wird im aktuellen Verzeichnis ein
Unterverzeichnis mit Namen `Umgebungsname` angelegt,
worin sich dann alle benötigten Ressourcen dieser Umgebung befinden.

Der Einfachheit halber wird als Umgebungsname meistens
`venv` verwendet. Das Anlegen einer solchen Umgebung
geht dann also mit dem Befehl

    python -m venv venv

vonstatten und dauert ein paar Sekunden.


## Virtual Environment aktivieren

Ist die virtuelle Umgebung
– in den folgenden Beispielen wird der Name `venv` angenommen –
angelegt, kann sie auf Linux-, MacOS- oder Unix-Systemen mit dem Befehl

`. venv/bin/activate` (Punkt am Zeilenanfang beachten!)

oder auch

`source venv/bin/activate`

aktiviert werden.

Auf Windows-Systemen wird hingegen der Befehl

`venv\Scripts\activate`

verwendet.

Nach dem Aktivieren wird üblicherweise der Umgebungsname in Klammern
am Start des Prompts angezeigt, z.B.:

    user@linux:~$ source venv/bin/activate
    (venv) user@linux:~$


## Virtual Environment deaktivieren

Zum Deaktivieren einer virtuellen Umgebung
wird in allen Fällen der Befehl `deactivate` verwendet.

Die Umgebung kann danach jederzeit mit dem passenden
Aktivierungsbefehl (`. venv/bin/activate` oder `venv\Scripts\activate`)
wieder aktiviert werden und steht dann mit den dort installierten
Paketen wieder zur Verfügung.


## Virtual Environment vollständig entfernen

Wenn eine virtuelle Umgebung nicht mehr gebraucht wird,
kann sie durch Löschung des entsprechenden Unterverzeichnisses
(in unserem Fall `venv`) einfach entfernt werden.
