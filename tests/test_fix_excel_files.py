# -*- coding: utf-8 -*-

"""

tests.test_fix_excel_files

Unit test the postleid.fix_excel_files module

Copyright (C) 2023 Rainer Schwarzbach

This file is part of postleid.

postleid is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

postleid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""


import json

from unittest import TestCase

import pandas

from postleid import commons
from postleid import fix_excel_files
from postleid import presets


class CCLookup(TestCase):

    """Test country code lookup"""

    def test_lookup(self):
        """lookup country codes on the basis of an existing ISO-3166-1 file"""
        try:
            with open(
                "/usr/share/iso-codes/json/iso_3166-1.json",
                mode="r",
                encoding="utf-8",
            ) as iso_codes_file:
                iso_codes = json.load(iso_codes_file)
        except OSError as error:
            self.skipTest(f"Error opening ISO codes file: {error}")
            return
        #
        data_fixer = fix_excel_files.DataFixer(
            pandas.DataFrame({"Land": ["de", "gb", "fr"]}),
            commons.UserSettings(),
        )
        for country_data in iso_codes["3166-1"]:
            names = [country_data["name"]]
            try:
                names.append(country_data["official_name"])
            except KeyError:
                pass
            #
            alpha_2 = country_data["alpha_2"].lower()
            if alpha_2 == presets.DEFAULT_CC:
                expected_cc = ""
            else:
                expected_cc = alpha_2
            #
            existing_cc = data_fixer.lookup_country_code(alpha_2)
            if existing_cc == expected_cc:
                name = country_data["name"]
                with self.subTest(name=name, expected=expected_cc):
                    self.assertEqual(
                        data_fixer.lookup_country_code(name),
                        expected_cc,
                    )
                #
                try:
                    official_name = country_data["official_name"]
                except KeyError:
                    continue
                #
                with self.subTest(
                    official_name=official_name, expected=expected_cc
                ):
                    self.assertEqual(
                        data_fixer.lookup_country_code(official_name),
                        expected_cc,
                    )
                #
            #
        #


# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
