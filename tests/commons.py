# -*- coding: utf-8 -*-

"""

tests.commons

Common functionality for Unit tests

Copyright (C) 2023 Rainer Schwarzbach

This file is part of postleid.

postleid is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

postleid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""


import dataclasses
import io
import sys

from typing import Any, List, Optional, Tuple
from unittest.mock import patch

import polars

from postleid import commons as pl_commons
from postleid import testdata


ARG_COUNTRIES = "countries"
ARG_FIX = "fix"
ARG_LIST = "list"

ARG_RULES = "rules"
OPT_GUESS = "--guess-1000s"
OPT_QUIET = "--quiet"
OPT_VERSION = "--version"

RETURNCODE_OK = 0

SYS_STDIN = "sys.stdin"
SYS_STDERR = "sys.stderr"
SYS_STDOUT = "sys.stdout"

PATTERN_MATCHED_COUNTRIES = [
    "Netherlands",
    "Germany",
    "UK",
]
PATTERN_MATCHED_ZIPS = [
    "9876 XY",
    "12345",
    "NW1 8UH",
]
PATTERN_MATCHED_SCHEMA = [
    "Country",
    "Zip Code",
]
PATTERN_MATCHED_EVAL = (
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
)

FIXABLE_COUNTRIES = [
    None,
    "Germany",
    None,
]
FIXABLE_ZIPS = [
    "07895",
    "01234",
    "65432",
]
FIXABLE_SCHEMA = [
    "Country",
    "Zip Code",
]
FIXABLE_EVAL = (
    # all fixed because the source column is all numeric
    # and target data type is always string
    pl_commons.S_FIXED,
    pl_commons.S_FIXED,
    pl_commons.S_FIXED,
)

RG_STREETS = [
    "Bochowstraße",
    "Auf der Hohl",
    "Alfred-Döblin-Straße",
    "Anna-Paul-Straße",
    "ungültig",
    "Oberstraße",
    "Finkenhütte",
    "Heukers Weide",
    "Habichtstraße",
    "Altenburgstraße",
    "Obere Bergstraße",
    "In den Winkelwiesen",
    "Auf dem Gäu",
    "Anxbachstraße",
]
RG_CITIES = [
    "Unterkirnach",
    "Berschweiler",
    "Eßlingen",
    "Lehrte",
    "München",
    "Schöllnach",
    "Hahn",
    "Oberwolfach",
    "Oberhosenbach",
    "Westerhausen",
    "Imsweiler",
    "Kirschweiler",
    "Großenwiehe",
    "Lohne",
]
RG_PARTIAL_FIX_ZIPS = [
    "78.089",
    "55.608",
    "54636",
    "31.275",
    "80",
    "94.508",
    "56,850",
    "77709",
    "55758",
    "06484",
    "67808",
    "55743",
    "24969",
    "49393",
]
RG_FULL_FIX_ZIPS = [
    "78089",
    "55608",
    "54636",
    "31275",
    "80000",
    "94508",
    "56850",
    "77709",
    "55758",
    "06484",
    "67808",
    "55743",
    "24969",
    "49393",
]
RG_SCHEMA = [
    "Straße",
    "Postleitzahl",
    "Stadt",
]
RG_PARTIAL_FIX_EVAL = (
    pl_commons.S_OUT_OF_RANGE,
    pl_commons.S_OUT_OF_RANGE,
    pl_commons.S_UNCHANGED,
    pl_commons.S_OUT_OF_RANGE,
    pl_commons.S_OUT_OF_RANGE,
    pl_commons.S_OUT_OF_RANGE,
    pl_commons.S_OUT_OF_RANGE,
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_FIXED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
)
RG_FULL_FIX_EVAL = (
    pl_commons.S_FIXED,
    pl_commons.S_FIXED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_FIXED,
    pl_commons.S_FIXED,
    pl_commons.S_FIXED,
    pl_commons.S_FIXED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_FIXED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
    pl_commons.S_UNCHANGED,
)

UNFIXABLE_COUNTRIES = ["Zimbabwe", "Germany", "Denmark", "UK"]
UNFIXABLE_ZIPS = ["7895", "999", "65432", "8UH ZZ9"]
UNFIXABLE_SCHEMA = [
    "Country",
    "Zip Code",
]
UNFIXABLE_EVAL = (
    pl_commons.S_MISSING_RULES,
    pl_commons.S_OUT_OF_RANGE,
    pl_commons.S_WRONG_FORMAT,
    pl_commons.S_WRONG_FORMAT,
)


@dataclasses.dataclass
class ResultTable:

    """Table object with name,
    table contents as a sequence of sequences,
    a schema as a list of strings,
    an evaluation as Tuple of integers,
    and lazy dataframe cache
    """

    name: str
    data: List[Any]
    schema: List[str]
    evaluation: Tuple[int, ...]
    _dataframe_cache: Optional[polars.DataFrame] = dataclasses.field(
        init=False, default=None
    )

    @property
    def dataframe(self) -> polars.DataFrame:
        """Lazily load the DataFrame object"""
        if self._dataframe_cache is None:
            polars_obj = polars.from_records(self.data, schema=self.schema)
            if not isinstance(polars_obj, polars.DataFrame):
                raise ValueError("Expected a DataFrame definition!")
            #
            self._dataframe_cache = polars_obj
        #
        return self._dataframe_cache


TESTCASES: List[Tuple[testdata.ReprTable, bool, ResultTable]] = [
    (
        testdata.SOURCE_PATTERN_MATCHED,
        False,
        ResultTable(
            "pattern_matched",
            [PATTERN_MATCHED_COUNTRIES, PATTERN_MATCHED_ZIPS],
            PATTERN_MATCHED_SCHEMA,
            PATTERN_MATCHED_EVAL,
        ),
    ),
    (
        testdata.SOURCE_FIXABLE,
        False,
        ResultTable(
            "fixable",
            [FIXABLE_COUNTRIES, FIXABLE_ZIPS],
            FIXABLE_SCHEMA,
            FIXABLE_EVAL,
        ),
    ),
    (
        testdata.SOURCE_REQUIRING_GUESS,
        False,
        ResultTable(
            "requiring_guess_partial_fix",
            [RG_STREETS, RG_PARTIAL_FIX_ZIPS, RG_CITIES],
            RG_SCHEMA,
            RG_PARTIAL_FIX_EVAL,
        ),
    ),
    (
        testdata.SOURCE_REQUIRING_GUESS,
        True,
        ResultTable(
            "requiring_guess_full_fix",
            [RG_STREETS, RG_FULL_FIX_ZIPS, RG_CITIES],
            RG_SCHEMA,
            RG_FULL_FIX_EVAL,
        ),
    ),
    (
        testdata.SOURCE_UNFIXABLE,
        False,
        ResultTable(
            "fixable",
            [UNFIXABLE_COUNTRIES, UNFIXABLE_ZIPS],
            UNFIXABLE_SCHEMA,
            UNFIXABLE_EVAL,
        ),
    ),
]


@dataclasses.dataclass(frozen=True)
class GenericCallResult:

    """Result from a generic call"""

    returncode: int = RETURNCODE_OK
    stdout: str = ""

    @classmethod
    def do_call(cls, *args, **kwargs):
        """Abstract method: do the real function call"""
        raise NotImplementedError

    @classmethod
    def from_call(
        cls,
        *arguments,
        stdin_data=None,
        stdout=sys.stdout,
        stderr=sys.stderr,
        **kwargs,
    ):
        """Return a GenericCallResult instance
        from the real function call,
        mocking sys.stdin if stdin_data was provided.
        """
        assert stdout is sys.stdout
        assert stderr is sys.stderr
        if stdin_data is None:
            returncode = cls.do_call(*arguments, **kwargs)
        else:
            with patch(SYS_STDIN, new=io.StringIO(stdin_data)) as mock_stdin:
                assert mock_stdin is sys.stdin
                returncode = cls.do_call(*arguments, **kwargs)
            #
        #
        return cls(
            returncode=returncode,
            stdout=stdout.getvalue(),
        )


# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
