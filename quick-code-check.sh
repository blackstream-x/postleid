#!/bin/bash

# Quick code check
#
# Copyright (C) 2023 Rainer Schwarzbach
#
# This file is part of postleid.
#
# postleid is free software: you can redistribute it and/or modify
# it under the terms of the MIT License.
#
# postleid is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the LICENSE file for more details.
#

echo -e "\n=== mypy ===\n"
python3 -m mypy . --exclude venv || exit
echo -e "\n=== pylint (tests) ===\n--- ignoring duplicate-code ---\n"
PYTHONPATH=src pylint --disable=duplicate-code tests || echo "--- ignored issues in test modules ---"
echo -e "\n=== pylint (src)  ===\n--- ignoring fixme ---\n"
PYTHONPATH=src pylint --disable=fixme --reports=y src *.py || exit
echo -e "\n=== flake8 ===\n"
flake8 --exclude venv || exit
echo -e "\n=== black ===\n"
black -l 79 --check . || black -l 79 --diff .
