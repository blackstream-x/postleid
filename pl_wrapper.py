#!/usr/bin/env python

"""

pl_wrapper.py

Wrap the python -m postleid call, ensuring the virtualenv is set

Copyright (C) 2023 Rainer Schwarzbach

This file is part of postleid.

postleid is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

postleid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""


import os
import subprocess
import sys


ENV_PYTHONPATH = "PYTHONPATH"


def main() -> int:
    """Check if the prerequisites are matched,
    then wrap the module call
    """
    environment = dict(os.environ)
    virtual_env = environment.get("VIRTUAL_ENV", "")
    if not virtual_env:
        print(
            "Dieses Skript muss in einem aktiven"
            " Python Virtual Environment aufgerufen werden."
        )
        return 1
    #
    try:
        python_version = subprocess.run(
            ("python", "--version"),
            capture_output=True,
            check=True,
            close_fds=True,
            env=environment,
            encoding="utf-8",
        ).stdout.strip()
    except subprocess.CalledProcessError:
        print("Python-Interpreter nicht gefunden.")
        return 1
    #
    if python_version.split()[1][0] < "3":
        print(f"Python 3.x (statt {python_version}) wird benötigt.")
        return 1
    #
    sources_dir = "src"
    wrapped_command = ["python", "-m", "postleid"] + sys.argv[1:]
    print(f"Setze Umgebungsvariable {ENV_PYTHONPATH} auf {sources_dir!r}")
    environment[ENV_PYTHONPATH] = sources_dir
    print(f"und rufe den Befehl {' '.join(wrapped_command)!r} auf")
    return subprocess.run(
        wrapped_command,
        capture_output=False,
        check=False,
        close_fds=True,
        env=environment,
    ).returncode


if __name__ == "__main__":
    sys.exit(main())


# vim: fileencoding=utf-8 sw=4 ts=4 sts=4 expandtab autoindent syntax=python:
