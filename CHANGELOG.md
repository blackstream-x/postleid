# Changelog

## v0.7.0

_2023-04-14_

- Aufruf des Befhls mit Subcommands (`fix` oder `list`)
- Unterstützung für folgende Länder hinzugefügt:
    - `[bl]` (Collectivité territoriale de) Saint-Barthélemy / St. Barth(s) / Saint-Barth / St. Barts / (Gebietskörperschaft) Saint-Barthélemy
    - `[es]` (Kingdom of) Spain / (Reino de) España / (Regne d')Espanya / Espainia / Espainiako Erresuma / (Reiaume d')Espanha / (Königreich) Spanien
    - `[gs]` South Georgia and the South Sandwich Islands / South Georgia / South Sandwich Islands / SGSSI / Südgeorgien und die Südlichen Sandwichinseln / Südgeorgien / Südliche Sandwichinseln
    - `[kn]` Saint Kitts and Nevis / (Federation of) Saint Christopher and Nevis / SKN / SKAN / St. Kitts / Saint Kitts / St. Christopher / Saint Christopher / Nevis / (Föderation) St. Kitts und Nevis / Sankt Christopher und Nevis / Sankt Christopher
    - `[lc]` Saint Lucia / Sent Lisi / Sainte-Lucie / Iouanalao / St. Lucia
    - `[lk]` (Democratic Socialist Republic of) Sri Lanka / ශ්‍රී ලංකා / ශ්‍රී ලංකා ප්‍රජාතාන්ත්‍රික සමාජවාදී ජනරජය / Śrī Laṅkā (Prajātāntrika Samājavādī Janarajaya) / இலங்கை / இலங்கை சனநாயக சோசலிசக் குடியரசு / Ilaṅkai (Jaṉanāyaka Sōsalisak Kuṭiyarasu) / (Demokratische Sozialistische Republik) Sri Lanka
    - `[mf]` (Collectivity of) Saint Martin / (Collectivité de) Saint-Martin / (Gebietskörperschaft) Saint-Martin / St. Martin
    - `[pa]` (Republic) of Panama / (República) de Panamá / (Republik) Panama
    - `[pe]` (Republic of) Peru / (República del) Perú / Piruw (Ripuwlika) / Piruwxa Ripuwlika / Piruw (Suyu) / (Republik) Peru
    - `[pg]` (Independent State of) Papua New Guinea / (Independen Stet bilong) Papua Niugini / (Independen Stet bilong) Papua Niu Gini / (Unabhängiger Staat) Papua-Neuguinea
    - `[ph]` (Republic of the) Philippines / (Republika ng) Pilipinas / (Republik der) Philippinen
    - `[pk]` (Islamic Republic of) Pakistan / پاکِستان / اِسلامی جمہوریہ پاكِستان / (Islamische Republik) Pakistan
    - `[pm]` (Territorial Collectivity of) Saint-Pierre and Miquelon / (Collectivité territoriale de) Saint-Pierre et Miquelon / (Collectivité d'outre-mer de) Saint-Pierre-et-Miquelon / Saint-Pierre und Miquelon / Saint-Pierre / Miquelon
    - `[pn]` Pitcairn(, Henderson, Ducie and Oeno) Islands / Pitcairn / Henderson / Ducie / Oeno / Pitkern Ailen / Pitcairninseln
    - `[pr]` (Commonwealth of) Puerto Rico / (Estado Libre Asociado de) Puerto Rico / (Freistaat) Puerto Rico
    - `[ps]` (State of) Palestine / فلسطين / دولة فلسطين / (Staat) Palästina
    - `[pw]` (Republic of) Palau / (Beluu er a) Palau / (Republik) Palau
    - `[py]` (Republic of) Paraguay / (República del) Paraguay / (Tavakuairetã) Paraguái / (Tetã) Paraguái / (Republik) Paraguay
    - `[re]` (La) Réunion / (La) Rényon
    - `[ro]` Romania / România / Rumänien
    - `[rs]` (Republic of) Serbia / (Република) Србија / (Republika) Srbija / (Republik) Serbien
    - `[ru]` Russia / Russian Federation / Россия / Российская Федерация / Russland / Russische Föderation
    - `[sa]` (Kingdom of) Saudi Arabia / KSA / العربية السعودية / المملكة العربية السعودية / (Königreich) Saudi-Arabien
    - `[sd]` (Republic of the) Sudan / السودان / جمهورية السودان / (Republik) Sudan
    - `[se]` (Kingdom of) Sweden / (Konungariket) Sverige / (Königreich) Schweden
    - `[sg]` (Republic of) Singapore / (Republik) Singapura / 新加坡 / 新加坡共和国 / சிங்கப்பூர் குடியரசு / (Republik) Singapur
    - `[sh]` Saint Helena, Ascension and Tristan da Cunha / Saint Helena (and Dependencies) / Ascension / Tristan da Cunha / St. Helena, Ascension und Tristan da Cunha / St. Helena (und Nebengebiete)
    - `[si]` (Republic of) Slovenia / (Republika) Slovenija / (Republika) Slovenija / (Republik) Slowenien
    - `[sj]` Svalbard and Jan Mayen / Svalbard / Jan Mayen / Svalbard og Jan Mayen / Svalbard und Jan Mayen / Spitzbergen
    - `[sk]` Slovakia / Slovak Republic / Slovensko / Slovenská republika / Slowakei / Slowakische Republik
    - `[sm]` (Republic of) San Marino / (Repubblica di) San Marino / (Ripóbblica d') San Marein / (Most Serene Republic of) San Marino / (Serenissima Repubblica di) San Marino / (Republik) San Marino
    - `[sn]` (Republic of) Senegal / (République du) Sénégal / (Republik) Senegal
    - `[so]` (Federal Republic of) Somalia / (Jamhuuriyadda Federaalka) Soomaaliya / الصومال / جمهورية الصومال الفيدرالية / (Bundesrepublik) Somalia
    - `[tc]` Turks and Caicos Islands / TCI / Turks Islands / Caicos Islands / Turks- und Caicosinseln / Turks & Caicos
    - `[th]` (Kingdom of) Thailand / เมืองไทย / ราชอาณาจักรไทย / Ratcha-anachak Thai / (Königreich) Thailand
    - `[tj]` (Republic of) Tajikistan / (Ҷумҳурии) Тоҷикистон / (Jumhurii) Tojikiston / (Республика) Таджикистан / (Republik) Tadschikistan
    - `[tm]` Turkmenistan / Türkmenistan (Respublikasy) / (Republik) Turkmenistan / Turkmenien
    - `[tr]` Turkey / (Republic of) Türkiye / Türkiye Cumhuriyeti / T.C. / (Republik) Türkei
    - `[tt]` (Republic of) Trinidad and Tobago / Trinidad / Tobago / (Republik) Trinidad und Tobago
    - `[tu]` (Republic of) Tunisia / تونس / الجمهورية التونسية / Tunesische Republik / Tunesien
    - `[tw]` Taiwan / Republic of China / Chinese Taipei / ROC / 中華民國 / 中華臺北 / 中华台北 / 中華臺北 / 中华台北 / 臺灣 / 台灣 / Zhōnghuá Mínguó / Zhōnghuá Táiběi / Republik China / Republik China auf Taiwan / Chinesisch(es) Taipeh
    - `[tz]` (United Republic of) Tanzania / (Jamhuri ya Muungano wa) Tanzania / (Vereinigte Republik) Tansania
    - `[ua]` Ukraine / Україна / Ukraïna / Ukrajina / Ukraine
    - `[um]` United States Minor Outlying Islands
    - `[uy]` (Oriental Republic of) Uruguay / (República Oriental del) Uruguay / (Republik Östlich des) Uruguay
    - `[uz]` (Republic of) Uzbekistan / Oʻzbekiston (Respublikasi) / O'zbekiston (Respublikasi) / Ўзбекистон (Республикаси) / Узбекистан / (Republik) Usbekistan
    - `[va]` Vatican City (State) / (Stato della) Città del Vaticano / Civitas Vaticana / Status Civitatis Vaticanae / Status Civitatis Vaticanæ / (Staat) Vatikanstadt / (Staat der) Vatikanstadt / Vatikanstaat
    - `[vc]` Saint Vincent and the Grenadines / St. Vincent und die Grenadinen
    - `[ve]` (Bolivarian Republic of) Venezuela / (República Bolivariana de) Venezuela / (Bolivarische Republik) Venezuela
    - `[vi]` United States Virgin Islands / Virgin Islands of the United States / USVI / Amerikanische Jungferninseln
    - `[vn]` (Socialist Republic of) Vietnam / Viet Nam / SRV / (Cộng hòa Xã hội chủ nghĩa) Việt Nam / (Sozialistische Republik) Vietnam
    - `[wf]` (Territory of the) Wallis and Futuna (Islands) / (Territoire des) îles Wallis-et-Futuna / Wallis-et-Futuna / ʻUvea mo Futuna / 'Uvea mo Futuna / (Territorium der Inseln) Wallis und Futuna / Uvea und Futuna
    - `[ws]` (Independent State of) Samoa / Western Samoa / (Malo Saʻoloto Tutoʻatasi o) Sāmoa / Samoa i Sisifo / (Unabhängiger Staat) Samoa / Westsamoa
    - `[za]` (Republic of) South Africa / RSA / iRiphabhuliki yaseNingizimu Afrika / iRiphabhlikhi yoMzantsi Afrika / Riphabliki yaseMzantsi Afrika / (Republiek van) Suid-Afrika / (Repabliki ya) Afrika-Borwa / (Rephaboliki ya) Afrika Borwa / (Rephaboliki ya) Aforika Borwa / (Riphabliki ya) Afrika Dzonga / (Riphabliki ra) Afrika Dzonga / iRiphabhulikhi yaseNingizimu-Afrika / (Riphabuḽiki ya) Afurika Tshipembe / iRiphabliki yeSewula Afrika / (Republik) Südafrika
    - `[zm]` (Republic of) Zambia / (Republik) Sambia


## v0.6.5

_2023-04-12_

Unterstützung weiterer Länder:

- `[fm]` (Federated States of) Micronesia / FSM / (Föderierte Staaten von) Mikronesien
- `[gb]` United Kingdom / UK / Great Britain / England / Northern Ireland / Scotland / Wales / Vereinigtes Königreich / Großbritannien / Nordirland / Schottland
- `[ke]` (Republic of) Kenya / Jamhuri ya Kenya
- `[kg]` Kyrgyzstan / Kyrgyz Republic / Кыргыз Республикасы / Kirgisistan / Kirgistan / Kirgisische Republik
- `[kr]` South Korea / Republic of Korea / ROK / 대한민국 / Südkorea / Republik Korea
- `[kw]` (State of) Kuwait / الكويت / دولة الكويت / Staat Kuwait
- `[kz]` Kazakhstan / Қазақстан (Республикасы) / Kasachstan
- `[la]` Laos / Lao People's Democratic Republic / LPDR / Lao PDR / ສາທາລະນະລັດ ປະຊາທິປະໄຕ ປະຊາຊົນລາວ
- `[lb]` (Republic of) Lebanon / Lebanese Republic / الجمهورية اللبنانية / Libanon
- `[lr]` (Republic of) Liberia
- `[ls]` (Kingdom of) Lesotho / Naha ea Lesotho
- `[lt]` (Republic of) Lithuania / Lietuva / Lietuvos Respublika / (Republik) Litauen
- `[lu]` (Grand Duchy of) Luxembourg / (Groussherzogtum) Lëtzebuerg / (Großherzogtum) Luxemburg
- `[lv]` Latvia / Latvija / Latveja / Lețmō / Lettland
- `[ma]` (Kingdom of) Morocco / المغرب / المملكة المغربية / (ⵜⴰⴳⵍⴷⵉⵜ ⵏ) ⵍⵎⴰⵖⵔⵉⴱ / ⵎⵓⵕⵕⴰⴽⵓⵛ / (Königreich) Marokko
- `[mc]` (Principality of) Monaco / Principauté de Monaco / (Prinçipatu de) Mu̍negu / Principato di Monaco / Fürstentum Monaco
- `[md]` (Republic of) Moldova / Republica Moldova / (Republik) Moldau / Moldawien
- `[me]` Montenegro / Crna Gora / Црна Гора
- `[mg]` (Republic of) Madagascar / (Repoblikan'i) Madagasikara / République de Madagascar / (Republik) Madagaskar
- `[mh]` (Republic of the) Marshall Islands / Aolepān Aorōkin Ṃajeḷ / (Republik) Marshallinseln
- `[mk]` (Republic of) North Macedonia / (Република) Северна Македонија / Republika e Maqedonisë së Veriut / Maqedonia e Veriut / (Republik) Nordmazedonien
- `[mm]` (Republic of the Union of) Myanmar / Burma / ပြည်ထောင်စု သမ္မတ မြန်မာနိုင်ငံတော်‌ / (Republik der Union) Myanmar / Birma
- `[mn]` Mongolia / ᠮᠤᠩᠭᠤᠯ ᠤᠯᠤᠰ / Монгол Улс / Mongolei / Mongolischer Staat
- `[mp]` (Commonwealth of the) Northern Mariana Islands / (Sankattan Siha Na) Islas Mariånas / (Commonwealth Téél Falúw kka Efáng) llól Marianas / Commonwealth der Nördlichen Marianen / Nördliche Marianen
- `[mq]` Martinique / Matinik / Matnik
- `[ms]` Montserrat
- `[mt]` (Republic of) Malta / Repubblika ta' Malta / (Republik) Malta
- `[mu]` (Republic of) Mauritius / मारीशस गणराज्य / (République) de Maurice / (Repiblik) Moris / Republik Mauritius
- `[mv]` (Republic of) Maldives / ދިވެހިރާއްޖޭގެ ޖުމްހޫރިއްޔާ / (Republik) Malediven
- `[mw]` (Republic of) Malawi / Dziko la Malaŵi / Charu cha Malaŵi / Republik Malawi
- `[mx]` Mexico / United Mexican States / México / Estados Unidos Mexicanos / Mexiko / Vereinigte Mexikanische Staaten
- `[my]` Malaysia
- `[mz]` (Republic of) Mozambique / (República de) Moçambique / Mozambiki / Msumbiji / Muzambhiki- / (Republik) Mosambik
- `[na]` (Republic of) Namibia / (Republiek van) Namibië / (Republik) Namibia / Namibiab Republiki dib / Orepublika yaNamibia / Republika zaNamibia / (Rephaboliki ya) Namibia / Namibia (ye Lukuluhile)
- `[nc]` New Caledonia / Nouvelle-Calédonie / Neukaledonien
- `[ne]` (Republic of the) Niger / (République du) Niger / (Jamhuriyar) Nijar / (Republik) Niger
- `[nf]` (Territory of) Norfolk Island / (Teratri a') Norf'k Ailen / Norfuk Ailen / Norfolkinsel
- `[ng]` (Federal Republic of) Nigeria / (Jamhuriyar Tarayyar) Najeriya / (Ọ̀hàńjíkọ̀ Ọ̀hànézè) Naìjíríyà / (Orílẹ̀-èdè Olómìniira Àpapọ̀) Nàìjíríà / (Bundesrepublik) Nigeria
- `[ni]` (Republic of) Nicaragua / (República de) Nicaragua / (Republik) Nicaragua
- `[nl]` Netherlands / Nederland / Niederlande
- `[no]` (Kingdom of) Norway / (Kongeriket) Norge / (Kongeriket) Noreg / Norga / Norgga gonagasriika / Vuodna / Vuona gånågisrijkka / Nöörje / Nöörjen gånkarïjhke / Norja / Norjan kuninkhaanvaltakunta / (Königreich) Norwegen
- `[np]` (Federal Democratic Republic of) Nepal / नेपाल / सङ्घीय लोकतान्त्रिक गणतन्त्र नेपाल / (Demokratische Bundesrepublik) Nepal
- `[nz]` New Zealand / Aotearoa / Neuseeland
- `[om]` (Sultanate of) Oman / (سلطنة) عمان / (Sultanat) Oman
- `[pl]` Poland / Polska / Rzeczpospolita Polska / Polen
- `[pt]` Portugal / República Portuguesa
- `[us]` United States (of America) / USA / Vereinigte Staaten (von Amerika)
- `[xk]` (Republic of) Kosovo / Kosova / Kosovë / Republika e Kosovës / (Република) Косово / Republik Kosovo
- `[yt]` (Department of) Mayotte / Département de Mayotte / Maore / Maori


## v0.6.4

_2023-04-11_

Unterstützung weiterer Länder:

- `[dk]` Denmark / Danmark / Dänemark
- `[do]` Dominican Republic / República Dominicana / Dominikanische Republik
- `[dz]` Algeria / الجزائر / Algerien
- `[ec]` Ecuador
- `[ee]` Estonia / Estland
- `[eg]` Egypt / مصر / Ägypten
- `[et]` Ethiopia / የኢትዮጵያ ፌዴራላዊ ዴሞክራሲያዊ ሪፐብሊክ / Äthiopien
- `[fi]` Finland / Suomi / Finnland
- `[fk]` Falkland Islands / Falklandinseln
- `[fo]` Faroe Islands / Føroyar / Färöer
- `[fr]` France / Frankreich
- `[ge]` Georgia / საქართველო / Georgien
- `[gf]` French Guiana / Guyane / Französisch-Guayana
- `[gg]` Guernsey
- `[gh]` Ghana / Republic of Ghana
- `[gi]` Gibraltar
- `[gl]` Greenland / Kalaallit Nunaat / Grönland
- `[gn]` Guinea
- `[gp]` Guadeloupe
- `[gr]` Greece / Ελληνική Δημοκρατία / Griechenland
- `[gt]` Guatemala
- `[gu]` Guam
- `[gw]` Guinea-Bissau
- `[hn]` Honduras
- `[hr]` Croatia / Hrvatska / Republika Hrvatska / Kroatien
- `[ht]` Haiti
- `[hu]` Hungary / Magyarország / Ungarn
- `[id]` Indonesia / Republik Indonesia / Indomesien
- `[ie]` Ireland / Éire / Irland
- `[il]` Israel / יִשְׂרָאֵל
- `[im]` Isle of Man
- `[in]` India / Bhārat Gaṇarājya / Indien
- `[io]` British Indian Ocean Territory
- `[iq]` Iraq / جمهورية العراق / Irak
- `[ir]` Iran / جمهوری اسلامی ایران
- `[is]` Iceland / Ísland / Island
- `[it]` Italy / Italia / Italien
- `[je]` Jersey
- `[jo]` Jordan / الأردن / المملكة الأردنية الهاشمية / Jordanien
- `[jp]` Japan / 日本 / 日本国
- `[kh]` Cambodia / កម្ពុជា / ព្រះរាជាណាចក្រកម្ពុជា / Kampuchea
- `[ky]` Cayman Islands
- `[li]` Liechtenstein
- `[pf]` French Polynesia / Polynésie française / Pōrīnetia Farāni / Französisch-Polynesien
- `[sv]` El Salvador
- `[sz]` Eswatini / Umbuso weSwatini
- `[vg]` British Virgin Islands / Britische Jungferninseln


## v0.6.3

_2023-04-06_

Benutzereinstellungen per Datei (`postleid-settings.yaml`)

## v0.6.2

_2023-04-05_

Erstes Release mit Unterstützung der Postleitzahlenregeln für folgende Länder:

- `[ad]` Andorra
- `[af]` Afghanistan
- `[ai]` Anguilla
- `[al]` Albania / Albanien
- `[am]` Armenia / Armenien
- `[aq]` British Antarctic Territory
- `[ar]` Argentina / Argentinien
- `[as]` American Samoa
- `[at]` Austria / Österreich
- `[au]` Australia / Australien
- `[ax]` Åland
- `[az]` Azerbaijan / Aserbaidschan
- `[ba]` Bosnia and Herzegovina / Bosnien-Herzegowina
- `[bb]` Barbados
- `[bd]` Bangladesh
- `[be]` Belgium / België / Belgie / Belgien
- `[bg]` Bulgaria / Bulgarien
- `[bh]` Bahrain
- `[bm]` Bermuda
- `[bn]` Brunei
- `[br]` Brazil / Brasilia / Brasilien
- `[bt]` Bhutan
- `[by]` Belarus
- `[ca]` Canada / Kanada
- `[cc]` Cocos Island
- `[ch]` Switzerland / Schweiz / Suisse
- `[cl]` Chile
- `[cn]` China / People's Republic of China / VR China / Volksrepublik China
- `[co]` Colombia / Kolumbien
- `[cr]` Costa Rica
- `[cu]` Cuba / Kuba
- `[cv]` Cape Verde / Cabo Verde / Kapverdische Inseln
- `[cx]` Chrismas Island
- `[cy]` Cyprus / Zypern
- `[cz]` Czechia / Czech Republic / Česko / Česká Republika / Tschechien / Tschechische Republik
- `[de]` Germany / Deutschland / Alemania / Allemagne

